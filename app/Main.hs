module Main where

import Data.Functor ((<&>))
import Data.List (intercalate)
import System.Random (randomRIO)

randomElement :: [a] -> IO a
randomElement ls = randomRIO (0, length ls - 1) <&> (ls !!)

randomLine :: FilePath -> IO String
randomLine fp = readFile fp <&> lines >>= randomElement

poemLines :: IO [String]
poemLines = traverse randomLine [ "data/column1.txt"
                                , "data/column2.txt"
                                , "data/column3.txt"
                                , "data/column4.txt"
                                ]

main :: IO ()
main = poemLines <&> intercalate "\n" >>= putStr
