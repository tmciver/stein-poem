# Stein Poem

This is a poem generator inspired by "Franny's Valentine Poem Generator" in the
children's book "Franny K. Stein, Mad Scientist, Attack of the 50 ft. Cupid."

![FrannyKSteinPoemGenerator](./img/franny-k-stein-poem-generator.jpg "Franny K Stein Poem Generator")

Customize the poem lines by adding to the "column" files in the `data`
directory.  Be sure to mind the meter and rhyme!

Enjoy!
